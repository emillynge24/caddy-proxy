#!/usr/bin/env sh
if [ -e /secrets/linode ] ;
then
    echo "found secret 'linode', adding LINODE_API_KEY to environment"
    export $(echo "LINODE_API_KEY=`cat /secrets/linode`")
fi

if [ -e /secrets/cloudflare ] ;
then
    echo "found secret 'cloudflare', adding CLOUDFLARE_API_KEY to environment"
    if [ -z "$CLOUDFLARE_EMAIL" ];
    then
        echo "CLOUDFLARE_EMAIL variable not set! cloudflare dns provider will not be available."
    else
        export $(echo "CLOUDFLARE_API_KEY=`cat /secrets/cloudflare`")
    fi
fi

if [ -e /secrets/cloudflare_token ] ;
then
    echo "found secret 'cloudflare', adding CLOUDFLARE_API_TOKEN to environment"
    export $(echo "CLOUDFLARE_API_TOKEN=`cat /secrets/cloudflare_token`")
fi

echo "caddy $@"
exec caddy $@ 2>&1