#
# Builder
#
FROM caddy:2.4.0-builder AS builder
RUN  /usr/local/go/bin/go get -d -v github.com/caddyserver/caddy/v2@v2.4.0
RUN xcaddy build \
    --with github.com/caddy-dns/cloudflare

FROM nimmis/alpine:3.4
RUN apk update && apk upgrade && \
    apk add curl && \
    rm -rf /var/cache/apk/*
# install caddy
COPY --from=builder /usr/bin/caddy /usr/bin/caddy

ENV DOCKER_GEN_VERSION 0.7.3
ENV CADDY_OPTIONS ""
# Telemetry Stats
ENV ENABLE_TELEMETRY="$enable_telemetry"

RUN curl -sL -o docker-gen-linux-amd64-$DOCKER_GEN_VERSION.tar.gz https://github.com/jwilder/docker-gen/releases/download/$DOCKER_GEN_VERSION/docker-gen-linux-amd64-$DOCKER_GEN_VERSION.tar.gz \
 && tar -C /usr/local/bin -xvzf docker-gen-linux-amd64-$DOCKER_GEN_VERSION.tar.gz \
 && rm /docker-gen-linux-amd64-$DOCKER_GEN_VERSION.tar.gz

RUN touch /etc/Caddyfile
ADD etc /etc
ADD caddy-entrypoint.sh /usr/bin/caddy-entrypoint
ENV DOCKER_HOST unix:///tmp/docker.sock
